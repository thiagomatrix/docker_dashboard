
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('', include('docker.urls')),
    path('create', include('docker.urls')),
    path('edit', include('docker.urls')),
    path('delete', include('docker.urls')),
    path('admin/', admin.site.urls),
]
