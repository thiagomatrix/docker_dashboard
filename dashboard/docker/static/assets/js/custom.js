$( "#docker_form" ).on( "submit", function(event) {
 if ( event.isDefaultPrevented() ) {
   //formError();
 } else {
   event.preventDefault();
   submitForm();
 }
});
   VMasker(document.getElementById("port")).maskPattern("9999");

   var alert_success = document.getElementById("alert-success");
   var alert_danger= document.getElementById("alert-danger");
   var status_s = document.getElementById('status-s');
   var message_s = document.getElementById('message-s');
   var status_d = document.getElementById('status-d');
   var message_d = document.getElementById('message-d');

   function edit(name, id, status){
     debugger;

     var checkout = status.indexOf("Paused");

     var start = 'true';
     var message = "Deseja parar o container ";

     if(checkout > -1){
       start = 'false';
       message = "Deseja iniciar o container ";
     }
     Swal.fire({
      title: message,
      text: name,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sim'
    }).then((result) => {
      if (result.value) {
        submitForm(id, start);
      }
    })
   }

   function delet(name, id){
    Swal.fire({
      title: 'Deseja realmente apagar o container ?',
      text: name,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sim, quero apagar!'
    }).then((result) => {
      if (result.value) {
        submitForm(id);
      }
    })
   }
     
   function submitForm(id=null, status=null){

   var data, url;
   
   if (!id && !status){
      var name = $("#name").val();
      var port = $("#port").val();
      url = "./create";
      data = "&name=" + name + "&port=" + port;
   }else if (id && status){
      url = "./edit";
      data = "&id=" + id + "&status=" + status;
   }else if (id && !status){
      url = "./delete";
      data = "&id=" + id;
   }

   $.ajax({
       type: "POST",
       url: url,
       data: data,
   
       success : function(text){
       var json = JSON.parse(text);
       
      if (json.success == "success"){

      $('#create').modal('hide');
      

      if(json.message && json.status){
        status_s.innerHTML = json.status;
        message_s.innerHTML = json.message;
        Swal.fire(json.status,json.message,'success');
        //alert_success.style.display = 'block';
      }
       
       $("#table").load(location.href + " #table>*", "");

           } else {

            if(json.message && json.status){
              status_d.innerHTML = json.status;
              message_d.innerHTML = json.message;
              Swal.fire(json.status,json.message,'danger');
              //alert_danger.style.display = 'block';
            }
            
           }
       }
   });
}

// table_search(search,tr,indexSearch='0')
$('#pesquisar').keyup(function () {
table_search($('#pesquisar').val(),$('#table_docker tbody tr'),'012');
});