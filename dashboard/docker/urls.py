
from django.urls import path
from .views import docker_list, docker_create, docker_edit, docker_delete, docker_login

urlpatterns = [
    path('', docker_list),
    path('create', docker_create),
    path('edit', docker_edit),
    path('delete', docker_delete),
    path('login', docker_login),
]