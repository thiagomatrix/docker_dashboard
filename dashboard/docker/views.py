from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, HttpResponseRedirect
import platform
import os
import subprocess
import json

#  Lista se há conteiners disponíveis.'

def platform_verify():
    # Darwin = MacOSX | Windows = Windows | Linux = Linux
    os_platform = platform.system()
    # print(os_platform)
    return os_platform

def docker_login(user):
    # username = user.username
    # password = user.password
    # cmd = 'docker login -u {username} -p {password}'.format(**locals())
    return

def docker_verify():
    return os.system('docker --version')

def bashToJson(json):
    my_json = json.decode('utf8').replace('\\', '')
    my_json = my_json.replace('/\//g', '')
    my_json = my_json.replace('\\/', '')
    my_json = my_json.replace('"\n', ',')
    my_json = my_json.replace('\n', '"')
    my_json = my_json.replace("'''", '"')
    my_json = my_json.replace('\'', '')
    my_json = my_json.replace("''", '""')
    my_json = my_json.replace('"nginx -g daemon of…"', 'nginx')
    my_json = my_json.replace("'", '')
    my_json = my_json.replace("'" + '"', '')
    my_json = my_json.replace('"}"', '"}')
    my_json = my_json.replace('}{', '},{')
    my_json = my_json.replace(',"Mounts":"', '')
    my_json = my_json.replace('"0\"'+ '"', '"0"')
    my_json = '{"lista":[' + my_json + ']}'

    print(my_json)
    return my_json

@csrf_exempt
def docker_create(request):
    if request.method == 'POST':
        name = request.POST.get('name')
        port = request.POST.get('port')

        try:
            cmd = 'docker run -d --name {name} -p {port}:80 nginx'.format(**locals())
            subprocess.check_output(cmd, shell=True)
            payload = {
                "success": "success",
                "status": "Muito Bom",
                "message": "Docker Criado com Sucesso",
            }
            return HttpResponse(json.dumps(payload), status=200)
        except:
            payload = {
                        "success": "danger",
                        "status": "Oh Não!!",
                        "message": "Erro ao criar o Container"
                }
            return HttpResponse(json.dumps(payload))
    # docker run -d --name namenew -p 8081:80 nginx
    return HttpResponse('Tentativa de Violação', status=403)

@csrf_exempt
def docker_edit(request):
    if request.method == 'POST':
        idd = request.POST.get('id')
        status = request.POST.get('status')
        msn = 'Pausado'
         
        if status == 'true':
            try:
                cmd = 'docker pause ' + idd
                direct_output = subprocess.check_output(cmd, shell=True)
            except:
                payload = {
                        "success": "danger",
                        "status": "Oh Não!!",
                        "message": "Erro ao pausar o Container " + idd,
                }
                return HttpResponse(json.dumps(payload))
        else: 
            cmd = 'docker unpause ' + idd

            msn = 'Iniciado'

            try:
                direct_output = subprocess.check_output(cmd, shell=True)
            except:
                payload = {
                        "success": "danger",
                        "status": "Oh Não!!",
                        "message": "Erro ao iniciar o Container " + idd,
                }
                return HttpResponse(json.dumps(payload))

        payload = {
                "success": "success",
                "status": "Que Bom",
                "message": "O Container <strong>{idd}</strong> foi {msn} com Sucesso".format(**locals()),
            }
        return HttpResponse(json.dumps(payload), status=200)

@csrf_exempt
def docker_delete(request):
    if request.method == 'POST':
        idd = request.POST.get('id')
        cmd = 'docker rm {idd}  --force'.format(**locals())
        try:
            direct_output = subprocess.check_output(cmd, shell=True)
        except:
            payload = {
                    "success": "danger",
                    "status": "Oh Não!!",
                    "message": "Erro ao excluir o Container " + idd,
             }
            return HttpResponse(json.dumps(payload))

        payload = {
            "success": "success",
            "status": "Que Bom",
            "message": "O Container <strong>{idd}</strong> foi excluído com Sucesso".format(**locals()),
        }
        return HttpResponse(json.dumps(payload), status=200)
    # docker run -d --name namenew -p 8081:80 nginx
    return HttpResponse('Tentativa de Violação', status=403)
    

    # docker stop nameold
    # docker rm nameold

def docker_list(self):

    # docker container rm <hash> - Remove container
    # docker container stop <hash> - Stop shutdown container
    # docker container kill <hash> - Force shutdown container
    # docker container rm $(docker container ls -a -q) - Remove all containers
    # docker image ls -a  - List all images on this machine
    # docker image rm <image id> - Remove specified image from this machine
    # docker image rm $(docker image ls -a -q) - Remove all images from this machine
    # docker container create - create new container
    # docker container start -ai <name>
    # touch 'myfile.txt

    myplatform = platform_verify()
    service = docker_verify()
    message = 'docker rodando :)'
    url = ''
    status = 1
    data = ''
    notify = False

    if service == 32512 or service == 1: # Mac 32512 | Win 1
        message = 'Favor verificar se seu docker esteja instalado e rodando'
        status = 0
        notify = True
        print(service)
        if myplatform == 'Darwin':
            url = 'https://download.docker.com/mac/stable/Docker.dmg'
            myplatform = 'MacOSX'
        elif myplatform == 'Linux':
            url = 'https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-docker-ce'
        elif myplatform == 'Windows':
            url = 'https://download.docker.com/win/stable/Docker%20for%20Windows%20Installer.exe'
        print(notify)
        payload = {
                'os': myplatform,
                'msn': message,
                'status': status,
                'url': url,
                'notify': notify,
            }
        return render(self, 'docker_list.html', payload)
    if not notify:
        cmd = 'docker container  ls -a --format "{{json .}}"'
        try:
            direct_output = subprocess.check_output(cmd, shell=True)
        except:
             if myplatform == 'Darwin':
                url = 'https://download.docker.com/mac/stable/Docker.dmg'
                myplatform = 'MacOSX'
             elif myplatform == 'Linux':
                url = 'https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-docker-ce'
             elif myplatform == 'Windows':
                url = 'https://download.docker.com/win/stable/Docker%20for%20Windows%20Installer.exe'
             message = 'Seu docker está rodando, porém está com problemas. Tente reinicia-lo'
             status = 0
             notify = True
             payload = {
                    'os': myplatform,
                    'msn': message,
                    'status': status,
                    'url': url,
                    'notify': notify,
                    'dockers': data,
             }
             return render(self, 'docker_list.html', payload)

        my_json = bashToJson(direct_output)

        try:
            data = json.loads(my_json)
            # lista = json.dumps(data, indent=4, sort_keys=True)

        except:
            message = 'Há Container(s) em seu docker incompatível com o Docker Dashboard, favor verifique se seus container são do tipo Nginx'
            status = 0
            payload = {
                'os': myplatform,
                'msn': message,
                'status': status,
                'url': url,
                'notify': notify,
                'dockers': data,
            }
            return render(self, 'docker_list.html', payload)


    payload = {
        'os': myplatform,
        'msn': message,
        'status': status,
        'url': url,
        'notify': notify,
        'dockers': data,
    }

    return render(self, 'docker_list.html', payload)
