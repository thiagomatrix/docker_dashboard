@thiagobraga:

> Thiago Braga
> thiagomatrix@gmail.com

**Docker Manager - Dashboard**

Com a finalidade de tornar intuitivo os comandos executados pelo **Docker**, foi construído um dashboard para fazer o CRUD do docker de maneira interativa.

---

**Requerimentos e Recomendações**

Para a utilização desse projeto são necessários:

1. Docker Desktop
2. Python
3. Django
4. Certifique se que não há nenhum container existente

*Por motivo de segurança recomenda-se que os container sejam criados utilizado apenas o dashboard.*

---

**Compatibilidade do Projeto**

- No windows, verifique se nas configurações do Docker (docker -> settings -> Daemon) se a opção **Experimental** esteja habilitada.

Esse Dashboard é compatível para criação de container:

- Nginx

---

**Direitos Autorais**

Esse é um projeto Alpha e OpenSource, construido apenas para demostração.

*Por se tratar de um projeto Alpha, não é aconselhável o seu uso para ambiente de produção. O uso desse dashboard é estritamente de responsabilidade de seu usuário.*

---

## Instalação do Python

Instale o interpretador Python no seu computador..

1. Faça download do **Python 3.x** com variável de ambiente, no sistema operacional desejado. <br>Baixe agora o Python [Download Python](https://www.python.org/downloads/).
2. Feche e Abra o **Terminal**.
3. Execute o comando  `python --version.`
4. Verifique se a versão mostrada é a 3.xx.

---

## Clonar o repositorio

Para clonar o repositório, execute o comando `git clone URL` no terminar em um diretório que deseja copiar.  [URL](https://thiagomatrix@bitbucket.org/thiagomatrix/docker_dashboard.git)

---

## Criação da venv

Para encapsolamento dos módulos instaláveis, torna-se necessário a criação de um ambiente virtual 

### Instalação

1. Após a instalação do python 3.x, Instale o gerador de Ambientes Virtuais do Python: ` pip install virtualenv`. Para Mac ou Linux Execute em modo de Administrador com `sudo`
2. Logo após Execute o seguinte comando: `virtualenv -p python3 .venv`.
3. Na pasta raiz do diretório do projeto execute `python3 -m venv venv`.

### Ativação

#### a) Windows

- Entre na pasta da venv pelo terminal e execute `cd vens
activate`.

#### a) Mac ou Linux

1. No terminal execute **source** + **caminho do repositório** até a pasta bin da venv + **/activate**:  `source venv/bin/activate`
2. Para verificar se a venv está ativada, certifique que inicio do terminal esteja escrito (venv): `(venv) meu-computador$`

### Desativar Venv

-  dentro do terminal com (venv), execute o seguinte comando: `deactivate`

---

## Django

### Instalação

Todos os passos a partir daqui, os comandos deverão ser executados dentro de um ambiente virtual (venv).

-  Instale o  **Django**: `pip install django`.


### Execução

1. execute o comando `python3  dashboard/manage.py runserver` na pasta raiz do projeto
2. acesse o [link](http://localhost:8000) localhost:8000 